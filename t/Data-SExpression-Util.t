#!/usr/bin/perl
use strict;
use warnings;

use Test::More tests => 6;
use Data::Dump::Sexp qw/dump_sexp/;
BEGIN { use_ok('Data::SExpression::Util', ':all') };

my $list = cons 1, cons 2, cons 3, undef; # (1 2 3)
my $other_list = cons 4, cons 5, undef;	  # (4 5)

$list = append $list, $other_list; # $list is now (1 2 3 4 5)

is position(1, $list), 0;
is position(4, $list), 3;
ok !defined(position 0, $list);

$list = rev $list;
is dump_sexp($list), '(5 4 3 2 1)';

$list = mapcar { $_ + 1 } $list;   # (6 5 4 3 2)
is dump_sexp($list), '(6 5 4 3 2)';
